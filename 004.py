#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.
"""

def palindromes():
    pass

i = 999
j = 999
topnum = 0

while i > 99:
    j = 999
    while j > 99:
        num = i * j
        stringnum = str(num)
        # Given that max digits of 2 3-digit numbers is 6 (999 * 999 = 998001)
        # and min digits of 2 3-digit numbers is 5 (100 * 100 = 10000)
        if len(stringnum) == 6:
            if (stringnum[0] == stringnum[len(stringnum) - 1]) and (stringnum[1] == stringnum[len(stringnum) - 2]) and (stringnum[2] == stringnum[len(stringnum) - 3]):
                if num > topnum:
                    topnum = num
                print(num)
        elif len(stringnum) == 5:
            if (stringnum[0] == stringnum[len(stringnum) - 1]) and (stringnum[1] == stringnum[len(stringnum) - 2]):
                if num > topnum:
                    topnum = num
                print(num)
        j = j - 1
    i = i - 1
print("Highest: ",topnum)

# FOR 2-DIGIT NUMBERS (as a test of the algorithm):
i = 99
j = 99
while i > 9:
    while j > 9:
        num = i * j
        stringnum = str(num)
        # Given that max digits of 2 2-digit numbers is 4 (99 * 99 = 9801)
        # and min digits of 2 2-digit numbers is 3 (10 * 10 = 100)
        if len(stringnum) == 4:
            if (stringnum[0] == stringnum[len(stringnum) - 1]) and (stringnum[1] == stringnum[len(stringnum) - 2]):
                print(num)
                break
        if len(stringnum) == 5:
            if (stringnum[0] == stringnum[len(stringnum) - 1]):
                print(num)
                break
        j = j - 1
    i = i - 1