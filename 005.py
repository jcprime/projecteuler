#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
"""

num = 1
success = False

while success == False:
    div = 1
    while div <= 20:
        if num % div == 0:
            div = div + 1
        else:
            break
        #print(num, "%", div, "=", num % div)
    if div == 21:
        success = True
    else:
        num = num + 1
    #print(div)

print(num)