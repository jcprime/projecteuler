#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143?
"""

import math

orignum = 600851475143

def isprime(num):
    if num % 2 == 0:
        return False
    for test in range(3, num):
        if num % 2 != 0:
            if num % test == 0:
                return False
    return True

def listprimes(maxnum):
    primes = []
    for num in range(1, maxnum):
        if isprime(num) == True:
            #print(num)
            primes.append(num)
    return primes

primes = listprimes(int(math.sqrt(orignum)))

for guess in range(2, int(math.sqrt(orignum))):
    if guess in primes:
        if orignum % guess == 0:
            print(guess)

# Solution given by https://stackoverflow.com/questions/14138053/project-euler-3-with-python-most-efficient-method
n = 600851475143
i = 2
while i * i < n:
    while n % i == 0:
        n = n / i
    i = i + 1
print(n)