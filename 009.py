#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
a^2 + b^2 = c^2

For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.
"""

a = 1
b = 1
c = 1

while a < 1000:
    b = 1
    while b < 1000:
        c = 1
        while c < 1000:
            if a**2 + b**2 == c**2:
                #print(a, b, c)
                if a + b + c == 1000:
                    print(a, b, c, "product =", a*b*c)
            c = c + 1
        b = b + 1
    a = a + 1

#            print(a, b, c)