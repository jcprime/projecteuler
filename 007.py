#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

What is the 10001st prime number?
"""

testnum = 2
primes = []

while len(primes) < 10001:
    for num in primes:
        if testnum % num == 0 and testnum / num != 1:
            break
    else:
        primes.append(testnum)
    #print(num, testnum)
    testnum = testnum + 1

print(primes[10000])