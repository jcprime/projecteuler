#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
The sum of the squares of the first ten natural numbers is,
1^2 + 2^2 + ... + 10^2 = 385

The square of the sum of the first ten natural numbers is,
(1 + 2 + ... + 10)^2 = 552 = 3025

Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.

Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
"""

sumofsquares = 0
squareofsum = 0

for num in range(1, 101):
    sumofsquares = sumofsquares + (num**2)
    squareofsum = squareofsum + num

squareofsum = squareofsum**2
print("Sum of squares:", sumofsquares)
print("Square of sum:", squareofsum)

print("Difference:", squareofsum - sumofsquares)