#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.
"""

below = 2000000
total = 0
testnum = 2
primes = []

while testnum < below:
    for num in primes:
        if testnum % num == 0 and testnum / num != 1:
            break
    else:
        primes.append(testnum)
        total = total + testnum
        print(testnum, total)
    testnum = testnum + 1

print(total)
